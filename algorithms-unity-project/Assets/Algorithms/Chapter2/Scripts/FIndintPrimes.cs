using System.Text;
using System.Collections.Generic;
using UnityEngine;

public class FIndintPrimes : MonoBehaviour
{
    [SerializeField] int testNumber = 100;
    
    private void Start()
    {
        var primes = FindPrimes (testNumber);
        var builder = new StringBuilder();
        for (var i = 0; i < primes.Count - 1; i++)
            builder.Append($"{primes[i]}, ");
        if (primes.Count > 0)
            builder.Append($"{primes[primes.Count - 1]}.");
        Debug.Log(builder.ToString());
    }

    private List<int> FindPrimes(long max_number)
    {
        var is_composite = new bool[max_number + 1];
        for (int i = 0; i < is_composite.Length; i++)
            is_composite[i] = false;
        for (var i = 4; i <= max_number; i += 2)
            is_composite[i] = true;
        int next_prime = 2;
        int stop_at = (int)Mathf.Sqrt(max_number);
        while (next_prime <= stop_at)
        {
            for (var i = next_prime * 2; i <= max_number; i += next_prime)
                is_composite[i] = true;
            next_prime += 2;
            while (next_prime <= max_number && is_composite[next_prime])
                next_prime += 2;
        }

        var primes = new List<int>();
        for (int i = 2; i <= max_number; i++)
            if(!is_composite[i]) primes.Add(i);
        return primes;
    }
}
